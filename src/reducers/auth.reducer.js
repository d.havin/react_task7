import { LOGIN, LOGOUT } from '../constants/auth.constants'

export const defaultAuthState = {isAuth: false};

export default function ReducerAuthFunction (state = defaultAuthState, action) {
    switch (action.type) {
        case LOGIN: 
            return state = {isAuth: true}; 
        case LOGOUT: 
            return state = {isAuth: false}; 
        default:
            return state;
    }
};
