import { EDIT_USER, FETCH_USER_SUCCESS } from "../constants/user.constants";

const defaultUserState = {
  id: 1,
  name: "Ivan",
  surName: "Gavrikov",
  cartInfo: "3213 1231 4534 2342",
};

export default function profileReducer (state = defaultUserState, action) {
  switch (action.type) {
    case EDIT_USER:
      let newState = {
        name: action.data.name,
        surName: action.data.surName,
        cartInfo: action.data.cartInfo,
      };
      return newState;
    case FETCH_USER_SUCCESS:
      let fetchState = {
        name: action.data.name,
        email: action.data.email,
      };
      return fetchState;

    default:
      return state;
  }
};
