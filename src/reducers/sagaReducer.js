import {
  REQUESTED_USER,
  REQUESTED_USER_SUCCEEDED,
  REQUESTED_USER_FAILED,
} from "../constants/saga.constants";

const initialState = {
  url: [],
  loading: false,
  error: false,
};

const sagaReducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUESTED_USER:
      return {
        url: [],
        loading: true,
        error: false,
      };
    case REQUESTED_USER_SUCCEEDED:
      return {
        url: action.url,
        loading: false,
        error: false,
      };
    case REQUESTED_USER_FAILED:
      return {
        url: [],
        loading: false,
        error: true,
      };
    default:
      return state;
  }
};

export default sagaReducer;
