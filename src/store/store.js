import {createStore, combineReducers, applyMiddleware} from "redux";
import createSagaMiddleware from 'redux-saga'
import {watchFetchUser} from '../saga/saga'

import profileReducer from '../reducers/profile.reducer';
import productReducer from '../reducers/product.reducer';
import authReducer from '../reducers/auth.reducer'
import homeReducer from "../reducers/home.reducer";
import sagaReducer from "../reducers/sagaReducer";

const sagaMiddleware = createSagaMiddleware();

export const rootReducer = combineReducers({
    profile: profileReducer,
    product: productReducer,
    home: homeReducer,
    auth: authReducer,
    saga: sagaReducer
});

export const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(watchFetchUser); 