import React, { useState } from 'react';
import '../styles/profilePage.css';
import { editUser } from "../actionCreators/profile.action";
import { connect } from "react-redux";
import { fetchUser } from '../actionCreators/saga.action'
import ProfileInput from './unput.component';

const ProfileInfo = (props) => {
    const [editedName, setName] = useState('');
    const [editedSurName, setSurName] = useState('');
    const [editedCartInfo, setCartInfo] = useState('');

    // useEffect(() => {
    //     props.fetchUser();
    // }, []);

    const handleEdit = () => {
        let user = {
            name: editedName ? editedName : props.profile.name,
            surName: editedSurName ? editedSurName : props.profile.surName,
            cartInfo: editedCartInfo ? editedCartInfo : props.profile.cartInfo,
        }
        props.editUser(user);

        setName('');
        setSurName('');
        setCartInfo('');
    }

    const handleList = () => {

        props.fetchUser()
        console.log(props.saga.url)
    }

    // useMemo(() => {
    // }, [editedName, editedSurName, editedCartInfo])

    return (
        <div className="editInfoMenu">
            <h2 className="editUserTitle">Изменить данные</h2>
            <div className="userInfo"> Данные пользователя: {props.profile.name} {props.profile.surName} {props.profile.cartInfo}</div>
            Имя: <ProfileInput data={editedName} setInputData={setName} />
            Фамилия: <ProfileInput data={editedSurName} setInputData={setSurName} />
            Номер карты: <ProfileInput data={editedCartInfo} setInputData={setCartInfo} flag={true} />
            <button className="profButton" onClick={() => handleEdit()}>Изменить</button>
            <button className="profButton" onClick={() => handleList()}>Пользователь</button>
            {props.saga.loading
                ? <p>Loading...</p>
                : props.saga.error
                    ? <p>Error, try again</p>
                    : props.saga.url.map(user => (
                        <ul>
                            <li>
                                {user.name}
                            </li>
                        </ul>
                    ))
            }
        </div>
    )
}

const mapStateToProps = (state) => ({
    profile: state.profile,
    saga: state.saga
});

const mapDispatchToProps = (dispatch) => ({
    editUser: (data) => dispatch(editUser(data)),
    fetchUser: () => dispatch(fetchUser())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileInfo);

