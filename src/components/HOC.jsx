import React from 'react';
import { Login } from './LoginPage.component'
import { connect } from "react-redux";
import { Route } from 'react-router-dom';
import '../styles/profilePage.css';

function HOC({ component: Component, auth, ...rest }) {

    return <Route
        {...rest}
        render={props => {
            return (
                auth.isAuth ? (
                    <Component {...props} />
                ) : (<Login />)
            )
        }}
    />
}

const mapStateToProps = (state) => ({
    auth: state.auth
});

export default connect(
    mapStateToProps
)(HOC);
