import React, { useState} from 'react';
import '../styles/adminPage.css';
import {connect} from "react-redux";
import {addProduct, deleteProduct, editProduct} from "../actionCreators/product.action"

function AdminPage(props) {
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [isEdit, setIsEdit] = useState(false);
    const [img, setImg] = useState('');


    const handleClickAdd = () => {
        let product = {
            id: isEdit ? id : props.productState.length + 1,
            name: name,
            price: price,
            img : img,
            description: description
        }

        isEdit?
        props.editProduct(product):
        props.addProduct(product);
                
        setName('');
        setPrice('');
        setDescription('');
        setIsEdit(false);    
    }

    const handleEditProduct = (product) => {
        setIsEdit(true);
        setName(product.name);
        setPrice(product.price);
        setId(product.id);
        setImg(product.img);
        setDescription(product.description)
    }

    const handleDeleteProduct = (product) => {
        props.deleteProduct(product.id)
    }

    return (
        
        <div className="adminConteiner">
            <div className="adminMenu">
                <div>
                    <h2>Создать товар</h2>
                    <input value={name} type="text" placeholder="Название продукта" onChange={(e) => setName(e.target.value)}/>
                    <input value={description} type="text" placeholder="Описание продукта" onChange={(e) => setDescription(e.target.value)}/>
                    <input value={price} type="text" placeholder="Цена продукта" onChange={(e) => setPrice(e.target.value)}/>
                    <button className = "addButton" onClick={handleClickAdd}>Создать</button>
                </div>
            </div>   
            {
                props.product.map(product => (
                    <div className = "productBox" key={product.id}>
                        <div className = "staff">
                            <p className ="productName">{product.name}</p>
                            <img src = {product.img} alt = {product.name}></img>
                        </div>
                        <div className = "description">
                            <p className ="descriptionTitle">{product.name}</p>
                            {product.description}
                            <p>Цена: {product.price}</p>
                            <button className = "editButton" onClick={() => handleEditProduct(product)}>Изменить</button>
                            <button className = "deleteButton" onClick={() => handleDeleteProduct(product)}>Удалить</button>
                        </div>
                    </div>
                ))
            }
        </div>
    )
}

const mapStateToProps = (state) => ({
    product: state.product
});

const mapDispatchToProps = (dispatch) => ({
    addProduct: (data) => {dispatch(addProduct(data))},
    editProduct: (data) => {dispatch(editProduct(data))},
    deleteProduct: (id) => {dispatch(deleteProduct(id))},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AdminPage);

