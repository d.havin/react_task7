import React from 'react';  //useContext,
import HandleError from './HandleError'

 function ProfileInput(props) {
    // console.log(props.data.length) // вывод номера карты

    return (
        <div className="profileInput">
            <input value={props.data} type="text" onChange={(e) => props.setInputData(e.target.value)} />
            {props.flag && <HandleError length = {props.data.length}/>}
        </div>
    )  
}

export default React.memo(ProfileInput)


