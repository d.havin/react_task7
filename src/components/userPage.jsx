import React, { useState } from 'react';
import '../styles/userPage.css';
import { connect } from "react-redux";
import { searchProduct } from "../actionCreators/product.action";

function UserPage (props) {
    const [inputValue, setInputValue] = useState('');

    const handleSearch = () => {
    props.searchProduct(inputValue)
    }

    return (
        <>
        <div className = "searchMenu">
            <h2 className="findProduct">Найти товар</h2>
            Введите название продукта:<input value={inputValue} type="text" placeholder="Название продукта" onChange={(e) => setInputValue(e.target.value)}/>
            <button onClick={handleSearch}>Найти</button>
        </div>
        {
            props.product.map(object => (
                <div className = "generalProductBox" key={object.id}>
                    <div className = "staff">
                        <p className ="productName">{object.name} </p>
                        <img src={object.img } alt = {object.name}></img>
                    </div>
                    <div className = "description">
                        <p className ="descriptionTitle">{object.name} </p>
                        <div>{object.description}</div>
                        <p>Цена: {object.price}</p>
                    </div>
                </div>
            ))
        }
        </>
    ) 
}

const mapStateToProps = (state) => ({
    product: state.product
});

const mapDispatchToProps = (dispatch) => ({
    searchProduct: (data) => {dispatch(searchProduct(data))},
    // resetState: () => {dispatch(resetState())},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserPage);

