export const REQUESTED_USER = "REQUESTED_USER";
export const REQUESTED_USER_SUCCEEDED = "REQUESTED_USER_SUCCEEDED";
export const REQUESTED_USER_FAILED = "REQUESTED_USER_FAILED";
export const FETCHED_USER = "FETCHED_USER";