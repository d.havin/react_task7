import {
  requestUser,
  requestUserSuccess,
  requestUserError,
} from "../actionCreators/saga.action";
import { put, call, takeEvery } from "redux-saga/effects";

//генератор - наблюдатель
export function* watchFetchUser() {
  yield takeEvery("FETCHED_USER", fetchUserAsync); // или takeLatest
}

// worker-saga
function* fetchUserAsync() {
  try {
    yield put(requestUser());
    const data = yield call(() => {
      return fetch("https://jsonplaceholder.typicode.com/users").then((res) =>
        res.json()
      );
    });
    yield put(requestUserSuccess(data));
  } catch (error) {
    yield put(requestUserError());
  }
}

//https://jsonplaceholder.typicode.com/users

//https://dog.ceo/api/breeds/image/random
