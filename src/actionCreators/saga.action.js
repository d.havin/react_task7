import { REQUESTED_USER, REQUESTED_USER_SUCCEEDED, REQUESTED_USER_FAILED, FETCHED_USER } from "../constants/saga.constants";

export const requestUser = () => {
    return { type: REQUESTED_USER }
  };
  
// export  const requestUserSuccess = (data) => {
//     return { type: REQUESTED_USER_SUCCEEDED, url: data.message }
//   };

export  const requestUserSuccess = (data) => {
  return { type: REQUESTED_USER_SUCCEEDED, url: data }
};
  
export  const requestUserError = () => {
    return { type: REQUESTED_USER_FAILED }
  };
  
export  const fetchUser = () => {
    return { type: FETCHED_USER }
  }; 