import { EDIT_USER } from "../constants/user.constants";

// export const fetchUsersSuccess = (data) => ({type: FETCH_USER_SUCCESS, data});

export const editUser = (data) => ({type: EDIT_USER, data});