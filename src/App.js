import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import {store} from "./store/store";
import { Provider } from "react-redux";

import Header from "./components/header.component";
import ProfileInfo from "./components/profilePage";
import UserPage from "./components/userPage";
import AdminPage from "./components/adminPage";
import HOC from "./components/HOC";

function App() {
  return (
    <Provider store={store}>
    <BrowserRouter>
        <Header />
        <Route path="/adminPage" component={AdminPage} />
        <Route path="/userPage" component={UserPage} />
        <HOC exact path="/userProfile" component={ProfileInfo}/>
    </BrowserRouter>
    </Provider>
  );
}

export default App;
